#!/bin/bash
source $HOME/.config/user-dirs.dirs
touch  $XDG_DESKTOP_DIR/computer.desktop
echo -e "[Desktop Entry]\nType=Application\nIcon=system\nName=Computer\nGenericName=Computer\nComment=Open Computer\nCategories=FileManager;Utility;Core;GTK;\nExec=pcmanfm computer:///\nStartupNotify=true\nTerminal=false\nMimeType=x-directory/normal;inode/directory;" >> $XDG_DESKTOP_DIR/computer.desktop
chmod +x $XDG_DESKTOP_DIR/computer.desktop


