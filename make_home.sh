#!/bin/bash
source $HOME/.config/user-dirs.dirs
touch  $XDG_DESKTOP_DIR/home.desktop
echo -e "[Desktop Entry]\nType=Application\nIcon=system-file-manager\nName=Home\nGenericName=Home Folder\nComment=Open Home folder\nCategories=FileManager;Utility;Core;GTK;\nExec=pcmanfm\nStartupNotify=true\nTerminal=false\nMimeType=x-directory/normal;inode/directory;" >> $XDG_DESKTOP_DIR/home.desktop
chmod +x $XDG_DESKTOP_DIR/home.desktop


