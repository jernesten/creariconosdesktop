#!/bin/bash
source $HOME/.config/user-dirs.dirs
touch  $XDG_DESKTOP_DIR/network.desktop
echo -e "[Desktop Entry]\nType=Application\nIcon=network-workgroup\nName=Network\nGenericName=Network\nComment=Open network\nCategories=FileManager;Utility;Core;GTK;\nExec=pcmanfm network:///\nStartupNotify=true\nTerminal=false\nMimeType=x-directory/normal;inode/directory;" >> $XDG_DESKTOP_DIR/network.desktop
chmod +x $XDG_DESKTOP_DIR/network.desktop


