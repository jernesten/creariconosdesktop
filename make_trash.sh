#!/bin/bash
source $HOME/.config/user-dirs.dirs
touch  $XDG_DESKTOP_DIR/trash.desktop
echo -e "[Desktop Entry]\nType=Application\nIcon=user-trash\nName=Trash\nGenericName=Trash\nComment=Open trash\nCategories=FileManager;Utility;Core;GTK;\nExec=pcmanfm trash:///\nStartupNotify=true\nTerminal=false\nMimeType=x-directory/normal;inode/directory;" >> $XDG_DESKTOP_DIR/trash.desktop
chmod +x $XDG_DESKTOP_DIR/trash.desktop


